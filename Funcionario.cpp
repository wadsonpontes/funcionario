#include <iostream>
#include <vector>
#include <sstream>
#include <string>

class Empresa {
	private:
		std::string nome;
		std::string cnpj;

	public:
		Empresa() {
			this->nome = "Empresa Grande, Grandes Problemas";
			this->cnpj = "0000-00";
		}

		std::string getNome() {
			return this->nome;
		}

		void setNome(std::string nome) {
			this->nome = nome;
		}

		std::string getCnpj() {
			return this->cnpj;
		}

		void setCnpj(std::string cnpj) {
			this->cnpj = cnpj;
		}
};

class Funcionario {
	private:
		std::string nome;
		Empresa empresa;
		std::string departamento;
		double salario;
		std::string data_de_admissao;

	public:
		Funcionario() {
			this->nome = "NomeDoFuncionário";
			this->departamento = "Novo";
			this->salario = 1048.00;
			this->data_de_admissao = "13/07/2020";
		}

		std::string getNome() {
			return this->nome;
		}

		void setNome(std::string nome) {
			this->nome = nome;
		}

		Empresa getEmpresa() {
			return this->empresa;
		}

		void setEmpresa(Empresa empresa) {
			this->empresa = empresa;
		}

		std::string getDepartamento() {
			return this->departamento;
		}

		void setDepartamento(std::string departamento) {
			this->departamento = departamento;
		}

		double getSalario() {
			return this->salario;
		}

		void setSalario(double salario) {
			this->salario = salario;
		}

		std::string getDataDeAdmissao() {
			return this->data_de_admissao;
		}

		void setDataDeAdmissao(std::string data_de_admissao) {
			this->data_de_admissao = data_de_admissao;
		}
};

int main(int argc, char *argv[]) {
	Empresa empresa;
	std::vector<Funcionario*> funcionarios;
	int numero_de_funcionarios;

	std::cout << "Quantos funcionários deseja adicionar? ";
	std::cin >> numero_de_funcionarios;

	for (int i = 0; i < numero_de_funcionarios; ++i) {
		Funcionario* funcionario = new Funcionario();
		funcionario->setEmpresa(empresa);
		funcionarios.push_back(funcionario);
	}

	for (auto it : funcionarios) {
		if (it->getDepartamento() == "Novo") {
			it->setSalario(it->getSalario() * 1.1);
		}
	}

	for (auto it : funcionarios) {
		std::cout << "Funcionario: ";
		std::cout << it->getNome();
		std::cout << " Salário: ";
		std::cout << it->getSalario();
		std::cout << std::endl;
	}

	return 0;
}